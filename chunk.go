package main

import (
	"net/http"
	"time"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.Header()["Content-Type"] = []string{"text/html; charset=utf-8"}
		w.Write([]byte(`
<!DOCTYPE html>
<div id="time"><div>
<script>
	var xhr = new XMLHttpRequest()
	xhr.open("GET", "http://localhost:8080/time")
	xhr.onreadystatechange = function () {
		if (this.response) {
			time.innerHTML = this.response
			console.log(this.response.length)
		}
	}
	xhr.send()
</script>
		`))
	})
	http.HandleFunc("/time", func(w http.ResponseWriter, r *http.Request) {
		w.Header()["Content-Type"] = []string{"application/octet-stream"}
		for {
			w.Write([]byte(time.Now().Format(http.TimeFormat) + "<br>"))
			w.(http.Flusher).Flush()
			time.Sleep(time.Second)
		}
	})
	http.ListenAndServe(":8080", nil)
}
